<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserListController;
use App\Http\Controllers\PhotoController;
use App\Http\Controllers\CreateUserController;

Route::middleware('admin')->group(function () {

    Route::get('create', [CreateUserController::class, 'create'])
    ->name('create');
    
    Route::post('create', [CreateUserController::class, 'store']);
    
    Route::get('/upload', [PhotoController::class, 'create'])
    ->name('upload');
    
    Route::post('/upload', [PhotoController::class, 'store']);
    
    //Route::view('list','list');
    Route::get('/list', [UserListController::class, 'show'])->name('list');
    });