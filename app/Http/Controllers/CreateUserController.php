<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use App\Http\Requests\StoreUserRequest;

use Illuminate\View\View;



class CreateUserController extends Controller
{
    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('createuser');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreUserRequest $request, UserService $userService)
    {
        $user = User::create();
        
        $user = $userService->createUser($request);

        return redirect(RouteServiceProvider::HOME);
    }
}
